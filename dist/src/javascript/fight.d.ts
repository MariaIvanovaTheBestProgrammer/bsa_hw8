export declare function fight({ ...firstFighter }: {
    [x: string]: any;
}, { ...secondFighter }: {
    [x: string]: any;
}): any;
export declare function getDamage(attacker: any, enemy: any): number;
export declare function getHitPower(fighter: any): number;
export declare function getBlockPower(fighter: any): number;
