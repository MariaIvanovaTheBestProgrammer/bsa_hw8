export declare const fighters: {
    _id: string;
    name: string;
    source: string;
}[];
export declare const fightersDetails: {
    _id: string;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;
}[];
