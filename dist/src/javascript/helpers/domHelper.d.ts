export declare function createElement({ tagName, className, attributes }: {
    tagName: any;
    className: any;
    attributes?: {} | undefined;
}): any;
