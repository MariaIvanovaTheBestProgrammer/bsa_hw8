"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.callApi = void 0;
const mockData_1 = require("./mockData");
const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;
function callApi(endpoint, method) {
    return __awaiter(this, void 0, void 0, function* () {
        const url = API_URL + endpoint;
        const options = {
            method,
        };
        return useMockAPI
            ? fakeCallApi(endpoint)
            : fetch(url, options)
                .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
                .then((result) => JSON.parse(atob(result.content)))
                .catch((error) => {
                throw error;
            });
    });
}
exports.callApi = callApi;
function fakeCallApi(endpoint) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = endpoint === 'fighters.json' ? mockData_1.fighters : getFighterById(endpoint);
        return new Promise((resolve, reject) => {
            setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
        });
    });
}
function getFighterById(endpoint) {
    const start = endpoint.lastIndexOf('/');
    const end = endpoint.lastIndexOf('.json');
    const id = endpoint.substring(start + 1, end);
    return mockData_1.fightersDetails.find((it) => it._id === id);
}
//# sourceMappingURL=apiHelper.js.map