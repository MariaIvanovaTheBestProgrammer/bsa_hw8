"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFighterDetails = exports.getFighters = void 0;
const apiHelper_1 = require("../helpers/apiHelper");
function getFighters() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const endpoint = 'fighters.json';
            const apiResult = yield (0, apiHelper_1.callApi)(endpoint, 'GET');
            return apiResult;
        }
        catch (error) {
            throw error;
        }
    });
}
exports.getFighters = getFighters;
function getFighterDetails(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const apiResult = yield (0, apiHelper_1.callApi)(`details/fighter/${id}.json`, "GET");
        return apiResult;
    });
}
exports.getFighterDetails = getFighterDetails;
//# sourceMappingURL=fightersService.js.map