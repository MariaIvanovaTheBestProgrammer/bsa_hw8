"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBlockPower = exports.getHitPower = exports.getDamage = exports.fight = void 0;
function fight(_a, _b) {
    var firstFighter = __rest(_a, []);
    var secondFighter = __rest(_b, []);
    let winner;
    let undamagedFirstFighter = Object.assign({}, firstFighter);
    let undamagedSecondFighter = Object.assign({}, secondFighter);
    while (firstFighter.health >= 0 || secondFighter.health >= 0) {
        if (firstFighter.health > 0) {
            secondFighter.health -= getDamage(firstFighter, secondFighter);
            console.log(`${secondFighter.name} health is ${secondFighter.health}`);
        }
        else {
            winner = undamagedSecondFighter;
            break;
        }
        if (secondFighter.health > 0) {
            firstFighter.health -= getDamage(secondFighter, firstFighter);
            console.log(`${firstFighter.name} health is ${firstFighter.health}`);
        }
        else {
            winner = undamagedFirstFighter;
            break;
        }
    }
    console.log(`Winner is ${winner.name}`);
    return winner;
}
exports.fight = fight;
function getDamage(attacker, enemy) {
    let damage = getHitPower(attacker) - getBlockPower(enemy);
    if (damage < 0) {
        console.log(`${enemy.name} blocks ${attacker.name} hit`);
        damage = 0;
    }
    console.log(`${attacker.name} hits ${enemy.name} with ${damage} damage`);
    return damage;
}
exports.getDamage = getDamage;
function getHitPower(fighter) {
    const { attack } = fighter;
    const criticalHitChance = Math.floor(Math.random() * 2) + 1;
    return attack * criticalHitChance;
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    const { defense } = fighter;
    const blockHitChance = Math.floor(Math.random() * 2) + 1;
    return defense * blockHitChance;
}
exports.getBlockPower = getBlockPower;
//# sourceMappingURL=fight.js.map