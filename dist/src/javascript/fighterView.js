"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFighter = void 0;
const domHelper_1 = require("./helpers/domHelper");
function createFighter(fighter, handleClick, selectFighter) {
    const { name, source } = fighter;
    const nameElement = createName(name);
    const imageElement = createImage(source);
    const checkboxElement = createCheckbox();
    const fighterContainer = (0, domHelper_1.createElement)({ tagName: 'div', className: 'fighter' });
    fighterContainer.append(imageElement, nameElement, checkboxElement);
    const preventCheckboxClick = (ev) => ev.stopPropagation();
    const onCheckboxClick = (ev) => selectFighter(ev, fighter);
    const onFighterClick = (ev) => handleClick(ev, fighter);
    fighterContainer.addEventListener('click', onFighterClick, false);
    checkboxElement.addEventListener('change', onCheckboxClick, false);
    checkboxElement.addEventListener('click', preventCheckboxClick, false);
    return fighterContainer;
}
exports.createFighter = createFighter;
function createName(name) {
    const nameElement = (0, domHelper_1.createElement)({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;
    return nameElement;
}
function createImage(source) {
    const attributes = { src: source };
    const imgElement = (0, domHelper_1.createElement)({ tagName: 'img', className: 'fighter-image', attributes });
    return imgElement;
}
function createCheckbox() {
    const label = (0, domHelper_1.createElement)({ tagName: 'label', className: 'custom-checkbox' });
    const span = (0, domHelper_1.createElement)({ tagName: 'span', className: 'checkmark' });
    const attributes = { type: 'checkbox' };
    const checkboxElement = (0, domHelper_1.createElement)({ tagName: 'input', className: '', attributes });
    label.append(checkboxElement, span);
    return label;
}
//# sourceMappingURL=fighterView.js.map