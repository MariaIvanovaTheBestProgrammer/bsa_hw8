"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showWinnerModal = void 0;
const modal_1 = require("./modal");
const fighterDetails_1 = require("./fighterDetails");
function showWinnerModal(fighter) {
    const bodyElement = (0, fighterDetails_1.createFighterDetails)(fighter);
    (0, modal_1.showModal)({ title: "Winner", bodyElement });
}
exports.showWinnerModal = showWinnerModal;
//# sourceMappingURL=winner.js.map