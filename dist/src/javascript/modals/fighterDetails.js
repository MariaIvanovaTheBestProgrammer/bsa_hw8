"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFighterDetails = exports.showFighterDetailsModal = void 0;
const domHelper_1 = require("../helpers/domHelper");
const modal_1 = require("./modal");
function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    (0, modal_1.showModal)({ title, bodyElement });
}
exports.showFighterDetailsModal = showFighterDetailsModal;
function createFighterDetails(fighter) {
    const { name, attack, health, defense, source } = fighter;
    const fighterDetails = (0, domHelper_1.createElement)({ tagName: 'div', className: 'modal-body' });
    const nameElement = (0, domHelper_1.createElement)({ tagName: 'span', className: 'fighter-name' });
    const attackElement = (0, domHelper_1.createElement)({ tagName: 'span', className: 'fighter-attack' });
    const defenseElement = (0, domHelper_1.createElement)({ tagName: 'span', className: 'fighter-defense' });
    const healthElement = (0, domHelper_1.createElement)({ tagName: 'span', className: 'fighter-health' });
    const imageElement = (0, domHelper_1.createElement)({ tagName: 'img', className: "fighter-image", attributes: {
            src: source
        } });
    nameElement.innerHTML = `<br>Name: ${name}`;
    attackElement.innerHTML = `<br>Attack: ${attack}`;
    defenseElement.innerHTML = `<br>Defense: ${defense}`;
    healthElement.innerHTML = `<br>Health: ${health}`;
    fighterDetails.append(imageElement);
    fighterDetails.append(nameElement);
    fighterDetails.append(attackElement);
    fighterDetails.append(defenseElement);
    fighterDetails.append(healthElement);
    return fighterDetails;
}
exports.createFighterDetails = createFighterDetails;
//# sourceMappingURL=fighterDetails.js.map