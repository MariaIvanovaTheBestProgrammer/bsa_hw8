"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFighterInfo = exports.createFighters = void 0;
const fighterView_1 = require("./fighterView");
const fighterDetails_1 = require("./modals/fighterDetails");
const domHelper_1 = require("./helpers/domHelper");
const fight_1 = require("./fight");
const winner_1 = require("./modals/winner");
const fightersService_1 = require("./services/fightersService");
function createFighters(fighters) {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map(fighter => (0, fighterView_1.createFighter)(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer = (0, domHelper_1.createElement)({ tagName: 'div', className: 'fighters' });
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
exports.createFighters = createFighters;
const fightersDetailsCache = new Map();
function showFighterDetails(event, fighter) {
    return __awaiter(this, void 0, void 0, function* () {
        const fullInfo = yield getFighterInfo(fighter._id);
        (0, fighterDetails_1.showFighterDetailsModal)(fullInfo);
    });
}
function getFighterInfo(fighterId) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield (0, fightersService_1.getFighterDetails)(fighterId);
        return response;
    });
}
exports.getFighterInfo = getFighterInfo;
function createFightersSelector() {
    const selectedFighters = new Map();
    return function selectFighterForBattle(event, fighter) {
        return __awaiter(this, void 0, void 0, function* () {
            const fullInfo = yield getFighterInfo(fighter._id);
            if (event.target.checked) {
                selectedFighters.set(fighter._id, fullInfo);
            }
            else {
                selectedFighters.delete(fighter._id);
            }
            if (selectedFighters.size === 2) {
                const winner = (0, fight_1.fight)(selectedFighters.values()[0], selectedFighters.values()[1]);
                (0, winner_1.showWinnerModal)(winner);
            }
        });
    };
}
//# sourceMappingURL=fightersView.js.map