"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.startApp = void 0;
const fightersService_1 = require("./services/fightersService");
const fightersView_1 = require("./fightersView");
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
function startApp() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            loadingElement.style.visibility = 'visible';
            const fighters = yield (0, fightersService_1.getFighters)();
            const fightersElement = (0, fightersView_1.createFighters)(fighters);
            rootElement.appendChild(fightersElement);
        }
        catch (error) {
            console.warn(error);
            rootElement.innerText = 'Failed to load data';
        }
        finally {
            loadingElement.style.visibility = 'hidden';
        }
    });
}
exports.startApp = startApp;
//# sourceMappingURL=app.js.map