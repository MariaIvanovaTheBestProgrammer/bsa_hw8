declare const presets: (string | {
    targets: {
        firefox: string;
        chrome: string;
    };
})[][];
declare const plugins: string[];
