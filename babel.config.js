const presets = [
  ["@babel/preset-typescript"]
];

const plugins = ["@babel/plugin-proposal-class-properties", "@babel/plugin-proposal-optional-chaining"];
  
module.exports = { 
  presets, 
  plugins 
};