"use strict";

export function fight({...firstFighter}, {...secondFighter}) {
  let winner;
  let undamagedFirstFighter = {...firstFighter};
  let undamagedSecondFighter = {...secondFighter};

  while (firstFighter.health >= 0 || secondFighter.health >= 0) {

      if (firstFighter.health > 0) {
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          console.log(`${secondFighter.name} health is ${secondFighter.health}`);
      } else {
          winner = undamagedSecondFighter;
          break;
      }
      if (secondFighter.health > 0) {
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          console.log(`${firstFighter.name} health is ${firstFighter.health}`);
      } else {
          winner = undamagedFirstFighter;
          break;
      }
  }

  console.log(`Winner is ${winner.name}`);
  return winner;
}

export function getDamage(attacker, enemy) {

  let damage = getHitPower(attacker) - getBlockPower(enemy);

  if (damage < 0) {
      console.log(`${enemy.name} blocks ${attacker.name} hit`);
      damage = 0;
  }
  console.log(`${attacker.name} hits ${enemy.name} with ${damage} damage`);

  return damage;
}

export function getHitPower(fighter) {

  const { attack } = fighter;
  const criticalHitChance = Math.floor(Math.random() * 2) + 1;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {

  const { defense } = fighter;
  const blockHitChance = Math.floor(Math.random() * 2) + 1;
  
  return defense * blockHitChance;
}
