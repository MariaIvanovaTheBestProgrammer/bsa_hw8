"use strict";

import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const { name, attack, health, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: "fighter-image", attributes: {
      src: source
    } })

  nameElement.innerHTML = `<br>Name: ${name}`;
  attackElement.innerHTML = `<br>Attack: ${attack}`;
  defenseElement.innerHTML = `<br>Defense: ${defense}`;
  healthElement.innerHTML = `<br>Health: ${health}`;

  fighterDetails.append(imageElement);
  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);

  return fighterDetails;
}
